

function getElement(elName) {
  return document.querySelector(elName);
}

function getElements(elName) {
  return document.querySelectorAll(elName);
}

function removeClass(el, className) {
  if (el.classList)
    el.classList.remove(className);
  else
    el.className = el.className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
}

function addClass(el, className) {
  if (el.classList)
    el.classList.add(className);
  else
    el.className += ' ' + className;
}

var slider;
slider = {

  // Not sure if keeping element collections like this
  // together is useful or not.
  el: {
    slider: getElement("#slider"),
    allSlides: getElements(".slide"),
    sliderNav: getElement(".slider-nav"),
    allNavButtons: getElements(".slider-nav > a")
  },

  timing: 800,
  slideWidth: 300, // could measure this

  // In this simple example, might just move the
  // binding here to the init function
  init: function () {
    this.bindUIEvents();
  },

  bindUIEvents: function () {
    //DOMElementList to Array
    var slideAnchorEls = [].slice.call(this.el.sliderNav.getElementsByTagName('a'));
    slideAnchorEls.forEach(function (el) {
      el.addEventListener('click', function (event) {
        slider.handleNavClick(event, this);
      });
    });

    // What would be cool is if it had touch
    // events where you could swipe but it
    // also kinda snapped into place.
  },
  handleNavClick: function (event, el) {
    event.preventDefault();
    var position    = +el.getAttribute('href').split("-").pop();
    var curScrolled = +this.el.slider.scrollLeft;
    this.scrollLeft(this.el.slider, curScrolled, position * this.slideWidth, 500, function() {
      console.log('OLE ANIMATION BEENDET!');
    });
    this.changeActiveNav(el);
  },
  changeActiveNav: function (el) {
    var allNavButtons = [].slice.call(this.el.allNavButtons);
    allNavButtons.forEach(function (element) {
      removeClass(element, 'active');
    });

    addClass(el, "active");
  },
  scrollLeft: function (element, startPos, endPos, duration, callback) {

    var start       = startPos,
        to          = endPos,
        change      = to - start,
        currentTime = 0,
        increment   = 20;
        requestID   = undefined;

    duration = (typeof(duration) === 'undefined') ? 500 : duration;

    function render() {
      // increment the time
      currentTime += increment;
      // find the value with the quadratic in-out easing function
      var val = easeInOutQuad(currentTime, start, change, duration);
      // move the document.body

      element.scrollLeft = val;

      // do the animation unless its over
      if (currentTime < duration) {
        requestAnimFrame(render);
      } else {
        if (callback && typeof(callback) === 'function') {
          // the animation is done so lets callback
          callback();
        }
      }
    }

    var requestAnimFrame = (function(){
      return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || function( callback ){ window.setTimeout(callback, 1000 / 60); };
    })();

    function stopRender () {
      if (requestID) {
        cancelAnimationFrame(requestID);
        requestID = undefined;
      }
    }

    function easeInOutQuad (t, b, c, d) {
      t /= d/2;
      if (t < 1) {
        return c/2*t*t + b
      }
      t--;
      return -c/2 * (t*(t-2) - 1) + b;
    }

    render();
  }

};

console.log(slider);

slider.init();

// https://codepen.io/BaylorRae/pen/ImGBC
// Originally added click links, so I ported over and re-wrote