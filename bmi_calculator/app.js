document.getElementById('bmi-button').addEventListener('click', function() {
  var height = document.getElementById('body-height').value.replace(',', '.');
  var weight = document.getElementById('weight-kg').value.replace(',', '.');
  var output = document.getElementById('output');

  height = height / 100;
  var bmi = weight / (height * height);

  output.innerText = 'Das Ergebnis ist: ' + bmi.toFixed(2);
});
