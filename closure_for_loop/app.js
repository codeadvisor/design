'use strict';

window.addEventListener('load', function() {
  var theWrongWay = function() {
    for(var x = 0; x <= 2; x++) {
      setTimeout(function() {
        console.log('----Ausgabe theWrongWay----')
        console.log(x);
      }, 1000);
    }
  }

  var theRightWay = function() {
    for(var x = 0; x <= 2; x++) {
      (function(x) {
        setTimeout(function() {
          console.log('----Ausgabe theRightWay----')
          console.log(x);
        }, 1000);
      }(x));
    }
  }

  theWrongWay();
  theRightWay();
});
