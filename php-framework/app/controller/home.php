<?php
namespace Controller;
use Core\Controller;
use Model\User;

class Home extends Controller
{
  public function index($firstName = '', $lastName = '') {
    $user = new User();
    $user->setFirstName($firstName);
    $user->setLastName($lastName);

    $this->view('home/index', ['name' => $user->getFullName()]);
  }
}
