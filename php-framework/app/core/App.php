<?php
namespace Core;

class App {

  private $name;
  private $url;
  protected $controller = 'home';
  protected $method     = 'index';
  protected $params     = [];

  public function __construct() {
    $this->url = $this->parseUrl();

    $this->setController();
    $this->setMethod();
    $this->setParams();

    call_user_func_array([$this->controller, $this->method], $this->params);
  }

  public function parseUrl() {
    if(isset($_GET['url'])) {
      return $url = explode('/', filter_var(
        rtrim($_GET['url'], '/'), FILTER_SANITIZE_URL)
      );
    }
  }

  public function setName($name) {
    $this->name = $name;
  }

  public function getName() {
    return $this->name;
  }

  private function setController() {
    $name = ucfirst($this->url[0]);
    unset($this->url[0]);
    $namespaceReference = '\\Controller\\' .$name;

    $this->controller = new $namespaceReference;
  }

  private function setMethod() {
    $methodName = isset($this->url[1]) ? $this->url[1] : null
    unset($this->url[1]);

    if(
      is_object($this->controller) &&
      isset($methodName) &&
      method_exists($this->controller, $methodName)
    ) {
      $this->method = $methodName;
    }
  }

  private function setParams() {
    $this->params = $this->url ? array_values($this->url) : [];
  }
}
