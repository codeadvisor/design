<?php
namespace Core;

class Controller {
    public function __construct() {}

    protected function model($model) {
      echo $model;
    }

    protected function view($view, $data = []) {
      require_once '../app/view/' . $view . '.php';
    }

}
