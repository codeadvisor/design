<?php
namespace Model;

class User {
  private $firstName;
  private $lastName;

  public function setLastName($name) {
    $this->lastName = $name;
  }

  public function getLastName() {
    return $this->lastName;
  }

  public function setFirstName($name) {
    $this->firstName = $name;
  }

  public function getFirstName() {
    return $this->firstName;
  }

  public function getFullName() {
    return $this->firstName . ' ' . $this->lastName;
  }
}
