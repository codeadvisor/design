function BankAccount(balance) {
  this._initialBalance = balance;
  this._withdraws = [];
}

BankAccount.prototype = {
  getBalance: function() {
    var balance = this._initialBalance;
    for(let withdraw of this._withdraws) {
      balance = balance - withdraw;
    }
    return balance;
  },
  withdraw: function(amount) {
    if(this.getBalance() - amount < 0) {
      console.log('FEHLER');
    } else {
      this._withdraws.push(amount);
    }
  }
};


let b = new BankAccount(1000);
b.withdraw(200);
b.withdraw(900);
console.log(b.getBalance());
console.log(b);