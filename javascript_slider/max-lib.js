(function (global) {

  var MaxLib = function () {
    return new MaxLib.init();
  }

  MaxLib.prototype = {
    toUpperCase: function (str) {
      return str.toUpperCase();
    },
    toLowerCase: function (str) {
      return str.toLowerCase();
    },
    isNullOrUndefined: function (value) {
      return typeof (value) === 'undefined' || value === null;
    },
    getElById: function (id) {
      this.element = this.document.getElementById(id);
      return this;
    },
    //TODO update for classes / array
    on: function(name, callback) {
      this.element.addEventListener(name, function(event) {
        callback(event);
      });
      return this;
    },
    addClass: function (classStr) {
      if (this.element !== null && !this.hasClass(this.element, classStr)) {
        this.element.className += ' ' + classStr;
      }
      return this;
    },
    removeClass: function (classStr) {
      if (this.element !== null && this.hasClass(this.element, classStr)) {
        this.element.className = this.element.className.replace(classStr, '').trim();
      }
      return this;
    },
    toggleClass: function (classStr) {

    },
    hasClass: function (el, className) {
      var classes = el.classList;
      for (var i = 0; i < classes.length; i++) {
        if (classes[i] === className) {
          return true;
        }
      }
      return false;
    },
    createElement: function (type) {
      return this.document.createElement(type);
    },
    createElementFromHTML: function (html = null) {
      if (typeof html !== 'string') {
        return undefined;
      }

      //wrapper element to create a new element
      var wrapperEl = this.createElement('div');
      wrapperEl.innerHTML = html;

      //return the firstChild of the wrapper, the original wrapper el isn't needed anymore
      return wrapperEl.firstChild;
    },
    append: function (el) {
      this.element.appendChild(el);
      return this;
    },
    prepend: function (el) {
      //TODO check if this is really working in all browsers!
      //empty element, has undefined children
      var firstChild = this.element.children[0];
      this.element.insertBefore(el, firstChild);
      return this;
    },
    appendHtml: function (html) {
      this.element.innerHTML += html;
      return this;
    },
    prependHtml: function (html) {
      var currentHtml = this.element.innerHTML;
      html += currentHtml;
      this.element.innerHTML = html;
      return this;
    },
    ajax: function (options, timeout = 5000, isAsync = true) {
      var self         = this,
          xhr          = getXhrObject(),
          method       = getValidMethod(options.method),
          url          = options.url ? options.url : null,
          data         = options.data ? options.data : null;

      if (!url) {
        throw new Error('missing url parameter in options object');
      }

      xhr.timeout = timeout;

      xhr.onerror = xhr.ontimeout = function(err) {
        console.log('an error occurred during the transaction');
        if (options.error) {
          options.error(err);
        }
      };

      xhr.onreadystatechange = function() {
        if (xhr.readyState === 4 && isSuccess(xhr.status)) {
          if (options.success) {
            options.success(xhr.response);
          }
        }
      };

      xhr.open(method, url, isAsync);
      setRequestHeader(options.contentType);
      xhr.send( data || null );


      function abort() {
        if (xhr) {
          xhr.abort();
        }
      }

      function getValidMethod(method) {
        return self.validRequestMethods.indexOf(method.toUpperCase()) !== -1 ? method : 'GET';
      }

      function isSuccess(httpStatusCode) {
        return httpStatusCode >= 200 && httpStatusCode < 300 || httpStatusCode === 304;
      }

      //TODO muss erweitert werden um multiple header
      function setRequestHeader(contentType) {
        if (contentType && contentType !== false) {
          xhr.setRequestHeader('Content-type', contentType)
        } else {
          xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        }
      }

      function getXhrObject() {
        try {
          return new window.XMLHttpRequest();
        } catch (e) {
          throw new Error('error during xhr object initialization');
        }

      }
    },
    fadeInElement: function (element, time) {
      element.style.opacity = 0;
      element.style.display = 'block';

      var last = +new Date();
      var tick = function () {
        element.style.opacity = +element.style.opacity + (new Date() - last) / time;
        last = +new Date();

        if (+element.style.opacity < 1) {
          (window.requestAnimationFrame && requestAnimationFrame(tick)) || setTimeout(tick, 16);
        } else {
          element.style.opacity = 1;
        }
      };

      tick();
    },
    fadeOutElement: function (element, time) {
      element.style.opacity = 1;
      element.style.display = 'block';

      var last = +new Date();
      var tick = function () {
        element.style.opacity = +element.style.opacity - (new Date() - last) / time;
        last = +new Date();

        if (+element.style.opacity > 0) {
          (window.requestAnimationFrame && requestAnimationFrame(tick)) || setTimeout(tick, 16);
        } else {
          element.style.display = 'none';
          element.style.opacity = 0;
        }
      };

      tick();
    },
    initSimpleLightBox: function () {

      //TODO: weitere Animationen einbauen
      //TODO: bildwechsel einbauen

      var self = this;
      var ovEl = createOverlay();
      bindClickEvents(getImageElements());


      function bindClickEvents(elementList) {
        for (el of elementList) {
          el.addEventListener('click', function (event) {
            var targetedEl = event.target;
            var newImg = self.createElement('img');
            var oldImage = ovEl.getElementsByTagName('img');

            if (oldImage.length > 0) {
              oldImage.remove();
            }

            newImg.src = targetedEl.src;
            newImg.alt = targetedEl.alt;

            ovEl.firstChild.append(newImg);
            self.fadeInElement(ovEl, 500);

            ovEl.addEventListener('click', function handlerFn() {
              this.removeEventListener('click', handlerFn);
              newImg.remove();
              self.fadeOutElement(ovEl, 400);
            });

            document.addEventListener('keyup', function handlerFn() {
              this.removeEventListener('keyup', handlerFn);
              newImg.remove();
              self.fadeOutElement(ovEl, 400);
            });
          });
        }
      }

      function getImageElements() {
        var elList = document.getElementsByClassName('ml_lightbox');
        if (elList.length === 0) {
          throw new Error('missing image elements with class ml_lightbox');
        }
        return elList;
      }

      function createOverlay() {
        var ovEl = self.createElementFromHTML('<div id="ml_lb_overlay"><div id="ml_lb_imageBox"></div></div>');
        ovEl.style.display = 'none';
        document.body.append(ovEl);
        return ovEl;
      }
    }
  };

  MaxLib.init = function () {
    var self = this;
    self.global = global;
    self.document = self.global.document ? self.global.document : null;
    self.location = self.global.location ? self.global.location : null;
    self.element = null;
    self.validRequestMethods = ['PUT', 'DELETE', 'POST', 'GET'];
  };

  MaxLib.init.prototype = MaxLib.prototype;
  global.MaxLib = global.ML$ = MaxLib;

}(window ? window : this));
