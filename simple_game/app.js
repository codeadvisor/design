'use strict';

/**
 * class Renderer
 */
class Renderer {

  constructor(element, dimension) {
    this.element   = element;
    this.dimension = dimension;
    this.setupBox();
  }

  setupBox() {
    let box = document.createElement('div');
    box.style.position   = 'absolute';
    box.style.top        = '20px';
    box.style.left       = '20px';
    box.style.background = 'red';
    box.style.width      = this.dimension + 'px';
    box.style.height     = this.dimension + 'px';
    box.style.transition = 'all 0.5s ease';

    this.element.appendChild(box);
    this.box = box;
  }

  render(position) {
    this.box.style.top = position + 'px';
  }
}

/**
 * class Box
 */
class Box {
  constructor(dimension) {
    this.position  = 0;
    this.speed     = 0;
    this.dimension = 40;
  }

  runLoop() {
    this.speed++;
    this.position = this.position + this.speed;
  }

  moveUp() {
    this.speed = -20;
  }
}


/**
 * class Game
 */
class Game {
  constructor(element) {
    this.renderer  = new Renderer(element, 40);
    this.box       = new Box(40);
    this.element   = element;
    this.isRunning = true;
    this.registerGameEvents();
  }

  registerGameEvents() {
    this.element.onclick = () => {
      this.box.moveUp();
    }
  }

  start() {
    let timer = setInterval(() => {
      this.box.runLoop();

      if(this.box.position < 0) {
        this.stop(timer);
        console.log('Oberer Rand erreicht - Game Over!');
      }
      if((this.box.position + this.box.dimension) > this.element.clientHeight) {
        this.stop(timer);
        console.log('Unterer Rand erreicht - Game Over!');
      }
      if(this.isRunning) {
        this.renderer.render(this.box.position);
      }
    }, 100);

    console.log(timer);
  }

  stop(intervalId) {
    this.isRunning = false;
    clearInterval(intervalId);
  }
}


let game = new Game(document.getElementById('game'));
game.start();