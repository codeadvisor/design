function loadScript(src, callback) {
  let script = document.createElement('script');
  script.src = src;

  script.onload = () => callback(null, script);
  script.onerror = () => callback(new Error(`Script load error for ${src}`));

  document.head.append(script);
}


function promisify(f) {

  return function (...args) {

    return new Promise((resolve, reject) => {
      function callback(err, result) { // our custom callback for f
        if (err) {
          return reject(err);
        } else {
          resolve(result);
        }
      }

      args.push(callback); // append our custom callback to the end of arguments
      f.call(this, ...args); // call the original function
    });
  };
};


// usage:
let loadScriptPromise = promisify(loadScript);

loadScriptPromise('http://code.jquery.com/jquery-3.3.1.js')
  .then(response => console.log(response));