/*--------------------------------Selectors ----------------------------------*/
var elements = document.getElementsByName('my-input');
console.log(elements);


var elements = document.querySelector('.makeItCursive');
console.log(elements);

var elements = document.querySelectorAll('.makeItCursive');
console.log(elements);



var heading = document.getElementById('heading');
heading.style.fontWeight = 'bold';
heading.innerHTML = 'HELLOOOOO';


var paragraphs = document.getElementsByTagName('p');
for(var prop in paragraphs) {

  if(paragraphs.hasOwnProperty(prop)) {
    var el = paragraphs[prop];

    //bad choice
    //el.setAttribute('style', 'font-weight: bold;');
    //good choice
    el.style.fontWeight = 'bold';
  }


}

var classes = document.getElementsByClassName('makeItCursive');

for(var prop in classes) {

  if(paragraphs.hasOwnProperty(prop)) {
    var el = classes[prop];
    el.style.fontStyle = 'italic';
  }
}
/*------------------------------Selectors ------------------------------------*/


/*--------------------------Read File from Input -----------------------------*/
var fileInput = document.getElementsByTagName('input')[0];
fileInput.addEventListener('change', function() {
  if (this.files && this.files[0]) {
    var myFile = this.files[0];
    var reader = new FileReader();
    //reader.readAsArrayBuffer(myFile);
    reader.readAsBinaryString(myFile);

    reader.addEventListener('load', function (e) {
      var str = e.target.result;
      console.log(str);
    });
  }
});
/*--------------------------Read File from Input -----------------------------*/


/*------------------------ String and ArrayBuffer ----------------------------*/
function ab2str(buf) {
  return String.fromCharCode.apply(null, new Uint16Array(buf));
}

function str2ab(str) {
  var buf = new ArrayBuffer(str.length*2); // 2 bytes for each char
  var bufView = new Uint16Array(buf);
  for (var i=0, strLen=str.length; i<strLen; i++) {
    bufView[i] = str.charCodeAt(i);
  }
  return buf;
}
/*------------------------ String and ArrayBuffer ----------------------------*/


/*--------------------------- Closure Example --------------------------------*/
var add = (function() {
    var counter = 0;

    return function() {
      return counter+= 1;
    }
})();
/*--------------------------- Closure Example --------------------------------*/
