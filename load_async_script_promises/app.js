function loadScript(src, callback) {
  let script = document.createElement('script');
  script.src = src;
  script.onload = () => callback(script);
  document.head.append(script);
}

loadScript('https://cdnjs.cloudflare.com/ajax/libs/lodash.js/3.2.0/lodash.js', script => {
  alert(`Cool, the ${script.src} is loaded`);
  alert( _ ); // function declared in the loaded script
});


//Das Ganze nochmal "gestapelt"
loadScript('1.js', step1);

function step1(error, script) {
  if (error) {
    handleError(error);
  } else {
    // ...
    loadScript('2.js', step2);
  }
}

function step2(error, script) {
  if (error) {
    handleError(error);
  } else {
    // ...
    loadScript('3.js', step3);
  }
}

function step3(error, script) {
  if (error) {
    handleError(error);
  } else {
    // ...continue after all scripts are loaded (*)
  }
};



//Mit Promises gelöst loadScript Functin
function loadScriptByPromise(src) {
  return new Promise(function(resolve, reject) {
    let script = document.createElement('script');
    script.src = src;

    script.onload = () => resolve(script);
    script.onerror = () => reject(new Error("Script load error: " + src));

    document.head.append(script);
  });
}


loadScriptByPromise("https://cdnjs.cloudflare.com/ajax/libs/lodash.js/3.2.0/lodash.js")
  .then(function(script) {
    console.log(script.src);
    return loadScriptByPromise("http://code.jquery.com/jquery-3.3.1.js");
  })
  .then(function(script) {
    console.log(script.src);
    return loadScriptByPromise("https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.9.1/underscore.js");
  })
  .then(function(script) {
    console.log(script.src);
    //Nutzung der Scripte
    console.log(jQuery);
    console.log(_);
  });