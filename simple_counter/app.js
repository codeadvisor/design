'use strict';

window.addEventListener('load', function() {

  var createCounter = function(counter) {
    function incrementCounter() {
      counter++;
      return counter;
    }
    return incrementCounter;
  }

  var countUp = createCounter(0);
  console.log(countUp());
  console.log(countUp());
});
