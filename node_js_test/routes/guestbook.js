let express        = require('express');
let router         = express.Router();
let FileHandler    = require('../src/FileHandler');
let GuestbookEntry = require('../src/GuestbookEntry');
let fh             = new FileHandler('./data.json', 'utf-8');
let entries        = [];

/**
 * @description
 * the intial/index route
 */
router.get('/', (req, res) => {
  entries = [];

  fh.readFile().then((data) => {
    for(let obj of JSON.parse(data)) {
      entries.push(new GuestbookEntry(obj.title, obj.content));
    }

    res.render('index', {
      entries: entries
    });
    res.end();
  }, (err) => {
    throw new Error('Something went wrong with the FileHandler');
  });
});

/**
 * @description
 * route to create a new guestbook entry
 */
router.post('/guestbook/new', (req, res) => {
  let content = req.body.content;
  let title   = req.body.title;

  let newEntry = new GuestbookEntry(title, content);
  entries.push(newEntry);

  //Schreibe Daten in Datei
  fh.write(entries);
  res.write(JSON.stringify(newEntry));
  res.end();
});

module.exports = router;