'use strict';

$(document).ready(function() {
  let form = $('#gb-entry-form');

  $(form).submit(function(event) {
    event.preventDefault();

    let title   = $('#title').val();
    let content = $('#content').val();

    $.ajax({
      url: '/guestbook/new',
      method: 'POST',
      data: {
        'title': title,
        'content': content
      },
      success: function(data) {
        let currentEntries = $('.gb-entries');
        let entry = JSON.parse(data);

        let html = `
        <div class="gb-entry">
          <h3>${entry.title}</h3>
          <p>${entry.content}</p>
        </div>`;
        
        $(currentEntries).append(html);
      }
    });
  });
});