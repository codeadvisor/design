//Externe Module
let express = require('express');
//Die eigentliche App - sollte nach Möglichkeit nicht durchgereicht werden
let app = express();
let bodyParser = require('body-parser');
// support json encoded bodies
app.use(bodyParser.json());
// support encoded bodies
app.use(bodyParser.urlencoded({ extended: true }));

//Setzen der Template Engine ejs unter Express
app.set('view engine', 'ejs');
//Setzen des Template-Ordners, in welchem die Vies abgelegt werden
app.set('views', './views');
//Setzen des public-Ordners - öffentliche Ressourcen
app.use(express.static('./public'));
//verwende bodyParser und setze ihn auf urlencoded
app.use(bodyParser.urlencoded({extended: true}));
//Holen der App-Routen
app.use(require('./routes/guestbook'));

app.listen(5000, () => {
  console.log('App wurde gestartet');
});
