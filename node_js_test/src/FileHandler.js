class FileHandler {
  constructor(filename, coding) {
    //fs (FileSystem) is a standard nodejs lib
    this.fs       = require('fs');
    this.filename = filename;
    this.coding   = coding;
  }

  async readFile() {
    return new Promise((resolve, reject) => {
      this.fs.readFile(this.filename, this.coding, (err, data) => {
        if(err) {
          reject(err);
        } else {
          resolve(data);
        }
      })
    });
  }

  write(data) {
    this.fs.writeFile('./data.json', JSON.stringify(data));
  }
}

module.exports = FileHandler;