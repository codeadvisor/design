'use strict';

window.addEventListener('load', function() {

  var createGame = function() {
    setTimeout(function() {
      var begin  = Date.now();
      var button = document.getElementById('reaction-button');
      var output = document.getElementById('output');

      button.innerText = 'Click now!';

      button.addEventListener('click', function handlerFn() {
        var end = Date.now();
        var diff = (end - begin) / 1000;

        output.innerText = diff + ' seconds';
        this.innerText = 'wait...';

        this.removeEventListener('click', handlerFn);
        createGame();

      });
    }, (Math.random() * 1000) + 2500);
  }

  createGame();

});
