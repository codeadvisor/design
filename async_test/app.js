'use strict';

window.addEventListener('load', function() {

  let doWork = function() {
    console.log('doWork: Starting');
    for(let x = 0; x <= 3000000000; x++) {
      //Aufwendige Berechnung
    }
    console.log('doWork: Finising');
  }

  doWork();

  //Sollte als zweites ausgeführt werden. Wird es aber nicht! JavaScript führt das erste
  //doWork() aus, was sehr lange dauert, es setzt den Timeout und danach führt es wieder
  //doWork() aus, was wieder sehr lange dauert. Erst danach wird 'Hallo' ausgegeben
  setTimeout(function() {
    console.log('Hallo');
  }, 1000)

  doWork();

});
